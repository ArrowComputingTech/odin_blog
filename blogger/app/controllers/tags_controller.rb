class TagsController < ApplicationController
  def index
    @tags = Tag.all
  end

  def show
    @tag = Tag.find(params[:id])
  end

  def destroy
    @tag = Tag.find(params[:id])
    puts tag_path(@tag)
    puts (@tag.id)
    puts tag_path(@tag)
    @tag.destroy()
    flash.notice = "Tag '#{@tag.name}' destroyed. =("
    redirect_to :tags
  end
end
